﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

namespace Vanilla.Release {
	public class VanillaPerspectiveBumper : BumperBase {

		[Header("[ 'Perspective' Bumper ]")]

		public Transform[] blocks;
		public Vector3[] fromPositions;
		public Vector3[] toPositions;

		public Quaternion fromRotation;
		public Quaternion toRotation;

		public float lerpSpeed;
		public float pauseBetweenBlocks;

		public Transform blockParent;
		public Transform cameraPivot;

		public Text slogan;

		public bool loadOrderSent;

		void Start() {
			StartAnimation();
		}

		public void MoveBlocksToStartPositions() {
			for (int i = 1; i < blocks.Length; i++) {
				blocks[i].position = fromPositions[i];
			}
		}

		public void MoveBlocksToEndPositions() {
			for (int i = 1; i < blocks.Length; i++) {
				blocks[i].position = toPositions[i];
			}
		}

		public void GetBlocks() {
			blocks = blockParent.GetComponentsInChildren<Transform>();
		}

		public void SaveFromPositions() {
			fromPositions = new Vector3[blocks.Length];

			for (int i = 1; i < blocks.Length; i++) {
				Vector3 temp = blocks[i].position;
				temp.y = 50.0f;
				fromPositions[i] = temp;
			}
		}

		public void SaveToPositions() {
			toPositions = new Vector3[blocks.Length];

			for (int i = 1; i < blocks.Length; i++) {
				toPositions[i] = blocks[i].position;
			}
		}

		public void StartAnimation() {
			StartCoroutine("Animate");
		}

		IEnumerator Animate() {
			float blockLengthSplit = pauseBetweenBlocks / (blocks.Length * 0.4f);

			yield return new WaitForSeconds(1);

			for (int i = blocks.Length - 1; i > 0; i--) {
				StartCoroutine(MoveBlock(i, lerpSpeed));

				yield return new WaitForSeconds(Random.value * pauseBetweenBlocks);

				pauseBetweenBlocks -= blockLengthSplit + 0.1f;
			}

			yield return new WaitForSeconds(0.5f);

			StartCoroutine(SpinCameraAround());

			yield return null;
		}

		IEnumerator SpinCameraAround() {
			float i = 0;
			float oSize = Camera.main.orthographicSize;

			while (i < 1) {
				cameraPivot.eulerAngles = Vector3.Lerp(new Vector3(0, 180, 0), Vector3.zero, i * i * (3.0f - 2.0f * i));

				Camera.main.orthographicSize = Mathf.Lerp(oSize, 8, i);

				Color sTextColor = slogan.color;

				sTextColor.a = (i * i) - 0.5f;

				slogan.color = sTextColor;

				i += Time.deltaTime * 0.3f;

				yield return null;
			}

			cameraPivot.eulerAngles = Vector3.zero;

			yield return new WaitForSeconds(1.0f);

			LoadNextSetup();
		}

		IEnumerator MoveBlock(int id, float seconds) {
			float i = 0;
			float rate = 1 / seconds;
			float iSin = 0;

			YieldInstruction blockYield = new WaitForEndOfFrame();

			while (i < 1) {
				iSin = Mathf.Sin(i * Mathf.PI * 0.5f);

				blocks[id].position = Vector3.Lerp(fromPositions[id], toPositions[id], iSin);
				blocks[id].rotation = Quaternion.Slerp(fromRotation, toRotation, iSin);

				i += Time.deltaTime * rate;
				yield return null;
			}

			// End of initial slide in

			yield return blockYield;

			blocks[id].position = toPositions[id];

			i = 0;

			rate *= 3;

			fromPositions[id] = blocks[id].position;
			toPositions[id] = blocks[id].position + (Vector3.up * 0.333f);

			yield return blockYield;
			yield return blockYield;

			while (i < 1) {
				iSin = Mathf.Sin(i * Mathf.PI * 0.5f);

				blocks[id].position = Vector3.Lerp(fromPositions[id], toPositions[id], iSin);
				blocks[id].rotation = Quaternion.Slerp(toRotation, fromRotation, iSin - 0.05f);

				i += Time.deltaTime * rate;
				yield return null;
			}

			blocks[id].position = toPositions[id];
			blocks[id].rotation = fromRotation;

			yield return null;
		}

		void Update() {
			if (Input.GetButton("A")) {
				base.BeginSkip();
			}
		}
	}
}