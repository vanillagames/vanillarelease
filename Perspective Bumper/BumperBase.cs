﻿using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Release {
	public class BumperBase : VanillaScene {

		[Header("[ Bumper Base ]")]

		[Header("Skip Configuration")]
		[Tooltip("Can this bumper be skipped?")]
		public bool skippable;

		[Tooltip("Has this bumper already been skipped?")]
		public bool skipTriggered;

		[Tooltip("Once triggered, how long do we wait until the onSkipCompleted event is fired?")]
		public float skipTimer;

		[Header("Scene Loading")]
		[Tooltip("Which SceneSetup should be loaded next?")]
		public VanillaSceneAsset nextSetup;

		[Tooltip("If ticked, the nextSetupName field will be used to load a Setup automatically once the bumper has ended or the skip timer is up.")]
		public bool nextSetupLoadedAutomaticallyAfterSkipCompletion;

		[ReadOnly]
		[Tooltip("This will become true when the SceneSetup instruction has been sent.")]
		public bool loadInstructionSent;

		[Header("Events")]
		[Tooltip("This event fires the moment a skip is activated, allowing for things like transition triggers and faders to fire.")]
		public UnityEvent onSkipBegan;

		[Tooltip("This event fires after a nominated amount of time has passed after a skip begins.")]
		public UnityEvent onSkipCompleted;

		/// <summary>
		/// This should be triggered when the player has nominated to skip the bumper.
		/// </summary>
		public virtual void BeginSkip() {
			if (skippable && !skipTriggered) {
				skipTriggered = true;

				onSkipBegan.Invoke();

				if (skipTimer > 0) {
					Invoke("SkipTimerUp", skipTimer);
				} else {
					SkipTimerUp();
				}
			} else {
				Error("This bumper isn't set to be skippable, or the skip has already been triggered. Be patient!");
			}
		}

		/// <summary>
		/// This will happen automatically if and when the skip timer runs out.
		/// </summary>
		private void SkipTimerUp() {
			onSkipCompleted.Invoke();

			if (nextSetupLoadedAutomaticallyAfterSkipCompletion) {
				LoadNextSetup();
			} else {
				Warning("The skip timer is up, but I'm not configured to load the [{0}] setup automatically so I hope you've organized it some other way...", nextSetup.name);
			}
		}

		/// <summary>
		/// This will happen automatically if or when the skip timer runs out, or alternatively can be triggered in a custom way unique to the bumper.
		/// </summary>
		public virtual void LoadNextSetup() {
			if (!loadInstructionSent) {
				loadInstructionSent = true;

				VanillaSceneLoader.i.Load(nextSetup);
			} else {
				Error("I've already triggered the [{0}] setup, so I won't try a second time.", nextSetup.name);
			}
		}
	}
}